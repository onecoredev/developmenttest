﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneCore.DevTest.Ioc
{
    using System;
    using System.Collections;
    using System.Reflection;

    using Castle.MicroKernel;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;

    /// <summary>
    /// The IocHelper interface.
    /// </summary>
    public interface IIocHelper
    {
        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        void Dispose();

        /// <summary>
        /// Installs the specified installers.
        /// </summary>
        /// <param name="installers">The installers.</param>
        void Install(params IWindsorInstaller[] installers);

        /// <summary>
        /// Installs from assembly.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        void InstallFromAssembly(Assembly assembly);

        /// <summary>
        /// Installs from assembly.
        /// </summary>
        /// <param name="typeOfAssembly">The type of assembly.</param>
        void InstallFromAssembly(Type typeOfAssembly);

        /// <summary>
        /// Adds the facility.
        /// </summary>
        /// <param name="facility">The facility.</param>
        /// <returns>The container</returns>
        IWindsorContainer AddFacility(IFacility facility);

        /// <summary>
        /// Adds the facility.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <returns>The container</returns>
        IWindsorContainer AddFacility<T>() where T : IFacility, new();

        /// <summary>
        /// Adds the facility.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="onCreate">The on create.</param>
        /// <returns>The container</returns>
        IWindsorContainer AddFacility<T>(Action<T> onCreate) where T : IFacility, new();

        /// <summary>
        /// Removes the child container.
        /// </summary>
        /// <param name="childContainer">The child container.</param>
        void RemoveChildContainer(IWindsorContainer childContainer);

        /// <summary>
        /// Resolves the specified service.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>The object</returns>
        object Resolve(Type service, IDictionary arguments);

        /// <summary>
        /// Resolves the specified service.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="argumentsAsAnonymousType">Type of the arguments as anonymous.</param>
        /// <returns>The object</returns>
        object Resolve(Type service, object argumentsAsAnonymousType);

        /// <summary>
        /// Resolves the specified service.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <returns>The object</returns>
        object Resolve(Type service);

        /// <summary>
        /// Resolves the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="service">The service.</param>
        /// <returns>The object</returns>
        object Resolve(string key, Type service);

        /// <summary>
        /// Resolves the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="service">The service.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>The object</returns>
        object Resolve(string key, Type service, IDictionary arguments);

        /// <summary>
        /// Resolves the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="service">The service.</param>
        /// <param name="argumentsAsAnonymousType">Type of the arguments as anonymous.</param>
        /// <returns>The object</returns>
        object Resolve(string key, Type service, object argumentsAsAnonymousType);

        /// <summary>
        /// Resolves the specified arguments.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="arguments">The arguments.</param>
        /// <returns>The object</returns>
        T Resolve<T>(IDictionary arguments);

        /// <summary>
        /// Resolves the specified arguments as anonymous type.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="argumentsAsAnonymousType">Type of the arguments as anonymous.</param>
        /// <returns>The object</returns>
        T Resolve<T>(object argumentsAsAnonymousType);

        /// <summary>
        /// Resolves the specified key.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>The object</returns>
        T Resolve<T>(string key, IDictionary arguments);

        /// <summary>
        /// Resolves the specified key.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="argumentsAsAnonymousType">Type of the arguments as anonymous.</param>
        /// <returns>The object</returns>
        T Resolve<T>(string key, object argumentsAsAnonymousType);

        /// <summary>
        /// Resolves this object.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <returns>The object</returns>
        T Resolve<T>();

        /// <summary>
        /// Resolves the specified key.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="key">The key.</param>
        /// <returns>The object</returns>
        T Resolve<T>(string key);

        /// <summary>
        /// Resolves all.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <returns>The objects</returns>
        T[] ResolveAll<T>();

        /// <summary>
        /// Resolves all.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <returns>The objects</returns>
        Array ResolveAll(Type service);

        /// <summary>
        /// Resolves all.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>The objects</returns>
        Array ResolveAll(Type service, IDictionary arguments);

        /// <summary>
        /// Resolves all.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="argumentsAsAnonymousType">Type of the arguments as anonymous.</param>
        /// <returns>The objects</returns>
        Array ResolveAll(Type service, object argumentsAsAnonymousType);

        /// <summary>
        /// Resolves all.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="arguments">The arguments.</param>
        /// <returns>The objects</returns>
        T[] ResolveAll<T>(IDictionary arguments);

        /// <summary>
        /// Resolves all.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="argumentsAsAnonymousType">Type of the arguments as anonymous.</param>
        /// <returns>The objects</returns>
        T[] ResolveAll<T>(object argumentsAsAnonymousType);

        /// <summary>
        /// Releases the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        void Release(object obj);

        /// <summary>
        /// Returns the inner WindsorContainer
        /// </summary>
        /// <returns>The container</returns>
        IWindsorContainer GetContainer();

        /// <summary>
        /// Disposes the container and creates a new one
        /// </summary>
        void ResetContainer();
    }
}
