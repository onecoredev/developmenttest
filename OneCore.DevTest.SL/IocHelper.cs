﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneCore.DevTest.Ioc
{
    using System;
    using System.Collections;
    using System.Linq;
    using System.Reflection;

    using Castle.MicroKernel;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;

    /// <summary>
    /// Esta clase implementa un singleton que contiene la instancia de WindsorContainer de la aplicacion
    /// en curso.
    /// </summary>
    public class IocHelper : IDisposable, IIocHelper
    {
        /// <summary>
        /// The _instance.
        /// </summary>
        private WindsorContainer instance;

        /// <summary>
        /// Initializes static members of the <see cref="IocHelper"/> class.
        /// </summary>
        static IocHelper()
        {
            Instance = new IocHelper();
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="IocHelper"/> class from being created.
        /// </summary>
        private IocHelper()
        {
            this.instance = new WindsorContainer();
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        public static IocHelper Instance { get; private set; }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
            this.Dispose(true);
        }

        /// <summary>
        /// Installs the specified installers.
        /// </summary>
        /// <param name="installers">The installers.</param>
        public void Install(params IWindsorInstaller[] installers)
        {
            this.instance.Install(installers);
        }

        /// <summary>
        /// Installs from assembly.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        public void InstallFromAssembly(Assembly assembly)
        {
            foreach (var type in assembly.GetTypes().Where(type => typeof(IWindsorInstaller).IsAssignableFrom(type)))
            {
                var constructor = type.GetConstructor(new Type[] { });
                object installer = constructor.Invoke(new object[] { });
                this.Install(installer as IWindsorInstaller);
            }
        }

        /// <summary>
        /// Installs from assembly.
        /// </summary>
        /// <param name="typeOfAssembly">The type of assembly.</param>
        public void InstallFromAssembly(Type typeOfAssembly)
        {
            foreach (
                Type type in
                    typeOfAssembly.Assembly.GetTypes().Where(type => typeof(IWindsorInstaller).IsAssignableFrom(type)))
            {
                ConstructorInfo constructor = type.GetConstructor(new Type[] { });
                object installer = constructor.Invoke(new object[] { });
                this.Install(installer as IWindsorInstaller);
            }
        }

        /// <summary>
        /// Adds the facility.
        /// </summary>
        /// <param name="facility">The facility.</param>
        /// <returns>The container</returns>
        public IWindsorContainer AddFacility(IFacility facility)
        {
            return this.instance.AddFacility(facility);
        }

        /// <summary>
        /// Adds the facility.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <returns>The container</returns>
        public IWindsorContainer AddFacility<T>() where T : IFacility, new()
        {
            return this.instance.AddFacility<T>();
        }

        /// <summary>
        /// Adds the facility.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="onCreate">The on create.</param>
        /// <returns>The container</returns>
        public IWindsorContainer AddFacility<T>(Action<T> onCreate) where T : IFacility, new()
        {
            return this.instance.AddFacility(onCreate);
        }

        /// <summary>
        /// Removes the child container.
        /// </summary>
        /// <param name="childContainer">The child container.</param>
        public void RemoveChildContainer(IWindsorContainer childContainer)
        {
            this.instance.RemoveChildContainer(childContainer);
        }

        /// <summary>
        /// Resolves the specified service.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>The object</returns>
        public object Resolve(Type service, IDictionary arguments)
        {
            return this.instance.Resolve(service, arguments);
        }

        /// <summary>
        /// Resolves the specified service.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="argumentsAsAnonymousType">Type of the arguments as anonymous.</param>
        /// <returns>The object</returns>
        public object Resolve(Type service, object argumentsAsAnonymousType)
        {
            return this.instance.Resolve(service, argumentsAsAnonymousType);
        }

        /// <summary>
        /// Resolves the specified service.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <returns>The object</returns>
        public object Resolve(Type service)
        {
            return this.instance.Resolve(service);
        }

        /// <summary>
        /// Resolves the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="service">The service.</param>
        /// <returns>The object</returns>
        public object Resolve(string key, Type service)
        {
            return this.instance.Resolve(key, service);
        }

        /// <summary>
        /// Resolves the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="service">The service.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>The object</returns>
        public object Resolve(string key, Type service, IDictionary arguments)
        {
            return this.instance.Resolve(key, service, arguments);
        }

        /// <summary>
        /// Resolves the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="service">The service.</param>
        /// <param name="argumentsAsAnonymousType">Type of the arguments as anonymous.</param>
        /// <returns>The object</returns>
        public object Resolve(string key, Type service, object argumentsAsAnonymousType)
        {
            return this.instance.Resolve(key, service, argumentsAsAnonymousType);
        }

        /// <summary>
        /// Resolves the specified arguments.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="arguments">The arguments.</param>
        /// <returns>The object</returns>
        public T Resolve<T>(IDictionary arguments)
        {
            return this.instance.Resolve<T>(arguments);
        }

        /// <summary>
        /// Resolves the specified arguments as anonymous type.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="argumentsAsAnonymousType">Type of the arguments as anonymous.</param>
        /// <returns>The object</returns>
        public T Resolve<T>(object argumentsAsAnonymousType)
        {
            return this.instance.Resolve<T>(argumentsAsAnonymousType);
        }

        /// <summary>
        /// Resolves the specified key.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>The object</returns>
        public T Resolve<T>(string key, IDictionary arguments)
        {
            return this.instance.Resolve<T>(key, arguments);
        }

        /// <summary>
        /// Resolves the specified key.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="argumentsAsAnonymousType">Type of the arguments as anonymous.</param>
        /// <returns>The object</returns>
        public T Resolve<T>(string key, object argumentsAsAnonymousType)
        {
            return this.instance.Resolve<T>(key, argumentsAsAnonymousType);
        }

        /// <summary>
        /// Resolves this object.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <returns>The object</returns>
        public T Resolve<T>()
        {
            return this.instance.Resolve<T>();
        }

        /// <summary>
        /// Resolves the specified key.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="key">The key.</param>
        /// <returns>The object</returns>
        public T Resolve<T>(string key)
        {
            return this.instance.Resolve<T>(key);
        }

        /// <summary>
        /// Resolves all.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <returns>The objects</returns>
        public T[] ResolveAll<T>()
        {
            return this.instance.ResolveAll<T>();
        }

        /// <summary>
        /// Resolves all.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <returns>The objects</returns>
        public Array ResolveAll(Type service)
        {
            return this.instance.ResolveAll(service);
        }

        /// <summary>
        /// Resolves all.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>The objects</returns>
        public Array ResolveAll(Type service, IDictionary arguments)
        {
            return this.instance.ResolveAll(service, arguments);
        }

        /// <summary>
        /// Resolves all.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="argumentsAsAnonymousType">Type of the arguments as anonymous.</param>
        /// <returns>The objects</returns>
        public Array ResolveAll(Type service, object argumentsAsAnonymousType)
        {
            return this.instance.ResolveAll(service, argumentsAsAnonymousType);
        }

        /// <summary>
        /// Resolves all.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="arguments">The arguments.</param>
        /// <returns>The objects</returns>
        public T[] ResolveAll<T>(IDictionary arguments)
        {
            return this.instance.ResolveAll<T>(arguments);
        }

        /// <summary>
        /// Resolves all.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="argumentsAsAnonymousType">Type of the arguments as anonymous.</param>
        /// <returns>The objects</returns>
        public T[] ResolveAll<T>(object argumentsAsAnonymousType)
        {
            return this.instance.ResolveAll<T>(argumentsAsAnonymousType);
        }

        /// <summary>
        /// Releases the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void Release(object obj)
        {
            this.instance.Release(obj);
        }

        /// <summary>
        /// Returns the inner WindsorContainer
        /// </summary>
        /// <returns>The container</returns>
        public IWindsorContainer GetContainer()
        {
            return this.instance;
        }

        /// <summary>
        /// Disposes the container and creates a new one
        /// </summary>
        public void ResetContainer()
        {
            this.instance.Dispose();
            this.instance = new WindsorContainer();
            System.GC.Collect();
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.instance.Dispose();
            }
        }

        public void SetContainer(IWindsorContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }
            lock (this)
            {
                if (this.instance != null)
                {
                    this.instance.Dispose();
                    GC.Collect();
                }
                this.instance = (WindsorContainer)container;
            }
        }
    }
}
