﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneCore.DevTest.Bll.Configuration
{
    public class ConfigWebApi : IConfigWebApi
    {
        public string Salt => ConfigurationManager.AppSettings["Salt"].ToString();
    }
}
