﻿using OneCore.DevTest.Bll.Dto;
using System.Collections.Generic;

namespace OneCore.DevTest.Bll
{
    public interface IUserBO
    {
        IEnumerable<UserDto> GetUsers();
        UserDto GetUser(int id);
        ResponseDto CreateUser(UserDto user);
        void UpdateUser(UserDto user);
        void DeleteUser(int id);
        ResponseDto LoginUser(string userName, string password);
    }
}