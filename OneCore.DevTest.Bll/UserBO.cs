﻿using AutoMapper.QueryableExtensions;
using OneCore.DevTest.Bll.Dto;
using OneCore.DevTest.Bll.Enum;
using OneCore.DevTest.Bll.Helpers;
using OneCore.DevTest.Dal.Data;
using System;
using System.Collections.Generic;
using System.Linq;


namespace OneCore.DevTest.Bll
{
    public class UserBO : IUserBO
    {
        #region Private attributes

        private DevTest_Entities _context;
        private IUserHelper _userHelper;
        private IEncryptHelper _encryptHelper;

        #endregion

        #region Constructors

        public UserBO(IUserHelper userHelper, DevTest_Entities context, IEncryptHelper encryptHelper)
        {
            _context = context;
            _userHelper = userHelper;
            _encryptHelper = encryptHelper;
        }

        #endregion


        public ResponseDto CreateUser(UserDto user)
        {
            if (_userHelper.UserExist(AutoMapper.Mapper.Map<UserDto, User>(user)))
            {
                return new ResponseDto
                {
                    Status = StatusResponse.Failed,
                    Message = "El Usuario Existe",
                    Response = null
                };
            }

            var userInserted = AutoMapper.Mapper.Map<UserDto, User>(user);
            userInserted.Password = _encryptHelper.EncodePassword(userInserted.Password);
            userInserted.Status = true;
            userInserted.CreationDate = DateTime.Now;
            _context.Users.Add(userInserted);
            _context.SaveChanges();

            return new ResponseDto
            {
                Status = StatusResponse.Success,
                Message = "Usuario Insertado Correctamente",
                Response = userInserted
            };
        }

        public void DeleteUser(int id)
        {
            var userDeleted = _context.Users.Find(id);
            userDeleted.Status = false;
            _context.SaveChanges();
        }

        public UserDto GetUser(int id)
        {
            var user = _context.Users.Where(u => u.Id == id).FirstOrDefault();


            return user != null ? AutoMapper.Mapper.Map<User, UserDto>(user) : null;
        }

        public IEnumerable<UserDto> GetUsers()
        {
            return _context.Users.ToList().Select(AutoMapper.Mapper.Map<User, UserDto>);

        }

        private IEnumerable<UserDto> toUserDto(List<User> list)
        {
            return list.Select(s => new UserDto
            {
                Id = s.Id,
                Email = s.Email,
                Status = "",
                Gender = s.Gender,
                Password = s.Password,
                UserName = s.UserName
            }).ToList();
        }

        public void UpdateUser(UserDto user)
        {
            var userUpdate = _context.Users.Find(user.Id);
            userUpdate.UserName = user.UserName;
            userUpdate.Email = user.Email;
            userUpdate.Gender = user.Gender;
            userUpdate.Password = _encryptHelper.EncodePassword(user.Password);
            _context.SaveChanges();

        }


        public ResponseDto LoginUser(string userName, string password)
        {
            var pass = _encryptHelper.EncodePassword(password);
            var user = _context.Users.Where(u => u.UserName.ToLower().Equals(userName) && u.Password.Equals(pass)).FirstOrDefault();

            if (user == null)
                return new ResponseDto
                {
                    Status = StatusResponse.Error,
                    Message = "Usuario o contraseña invalida",
                    Response = null
                };


            return new ResponseDto
            {
                Status = StatusResponse.Success,
                Message = "Usuario logueado correctamente",
                Response = user
            };
        }
    }
}
