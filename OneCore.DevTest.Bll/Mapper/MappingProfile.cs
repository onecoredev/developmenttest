﻿using AutoMapper;
using OneCore.DevTest.Bll.Dto;
using OneCore.DevTest.Dal.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneCore.DevTest.Bll.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UserDto, User>().ForMember(x => x.Id, y => y.ResolveUsing(z => z.Id))
               .ForMember(x => x.Email, y => y.ResolveUsing(z => z.Email))
               .ForMember(x => x.Status, y => y.ResolveUsing(z => StatusToBool(z.Status)))
               .ForMember(x => x.UserName, y => y.ResolveUsing(z => z.UserName))
               .ForMember(x => x.Password, y => y.ResolveUsing(z => z.Password))
               .ForMember(x => x.Gender, y => y.ResolveUsing(z => z.Gender));

            CreateMap<User, UserDto>().ForMember(x => x.Id, y => y.ResolveUsing(z => z.Id))
                .ForMember(x => x.Email, y => y.ResolveUsing(z => z.Email))
                .ForMember(x => x.Status, y => y.ResolveUsing(z => StatusToString(z.Status)))
                .ForMember(x => x.UserName, y => y.ResolveUsing(z => z.UserName))
                .ForMember(x => x.Password, y => y.ResolveUsing(z => z.Password))
                .ForMember(x => x.Gender, y => y.ResolveUsing(z => z.Gender));


        }

        private bool? StatusToBool(string status)
        {
            return status.Equals("Si") ? true : false;
        }

        private string StatusToString(bool? status)
        {
            return status == true ? "Si" : "No";
        }
    }
}
