﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneCore.DevTest.Bll.Helpers
{
    public interface IEncryptHelper
    {
        string EncodePassword(string password);

        string EncodePasswordMd5(string password);

        string base64Encode(string sData);

        string base64Decode(string sData);
    }
}
