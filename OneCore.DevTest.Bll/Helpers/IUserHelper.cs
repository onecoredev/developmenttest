﻿using OneCore.DevTest.Bll.Dto;
using OneCore.DevTest.Dal.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneCore.DevTest.Bll.Helpers
{
    public interface IUserHelper
    {
        bool UserExist(User userExist);
        UserDto FindUser(int id);
    }
}
