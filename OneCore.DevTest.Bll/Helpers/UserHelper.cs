﻿using OneCore.DevTest.Bll.Dto;
using OneCore.DevTest.Dal.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OneCore.DevTest.Bll.Helpers
{
    public class UserHelper : IUserHelper
    {
        DevTest_Entities _context;

        public UserHelper(DevTest_Entities context)
        {
            _context = context;
        }

        public bool UserExist(User userExist)
        {
            var user = _context.Users.Where(u => u.Email.ToLower().Equals(userExist.Email.ToLower()) || u.UserName.ToLower().Equals(userExist.UserName.ToLower())).FirstOrDefault();

            return user == null ? false : true;

        }

        public UserDto FindUser(int id)
        {
            var user = _context.Users.Find(id);
            return user != null ? AutoMapper.Mapper.Map<User, UserDto>(user) : null;
        }
        
    }
}
