﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneCore.DevTest.Bll.Dto
{
    public class UserDto
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Email es requerido")]
        [EmailAddress(ErrorMessage = "Email invalido")]
        public string Email { get; set; }

        [StringLength(50, ErrorMessage = "Minimo tamaño de {0} es {2}", MinimumLength = 7)]
        public string UserName { get; set; }

        [StringLength(50, ErrorMessage = "Minimo tamaño de {0} es {2}", MinimumLength = 10)]
        [RegularExpression(@"^((?=.*[a-z])(?=.*[A-Z])(?=.*\d))(?=.*[@_\-\\\/|$!%*?&#¿()=[\]{}¡+~^])[A-Za-z\d@_\\\-\/|$!%*?&#¿()=[\]{}¡+~^].+$", ErrorMessage = "El pass debe incluir al menos una letra mayuscula, una minuscula, un digito y un caracter.")]
        [Required(ErrorMessage = "Password requerido")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [StringLength(50, ErrorMessage = "Minimo tamaño de {0} es {2}", MinimumLength = 10)]
        [RegularExpression(@"^((?=.*[a-z])(?=.*[A-Z])(?=.*\d))(?=.*[@_\-\\\/|$!%*?&#¿()=[\]{}¡+~^])[A-Za-z\d@_\\\-\/|$!%*?&#¿()=[\]{}¡+~^].+$", ErrorMessage = "El pass debe incluir al menos una letra mayuscula, una minuscula, un digito y un caracter.")]
        [Required(ErrorMessage = "Confirmacion de password requerida")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Los password no coinciden")]
        public string PasswordConfirm { get; set; }

        public string Status { get; set; }

        public string Gender { get; set; }
    }
}
