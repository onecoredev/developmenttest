﻿using OneCore.DevTest.Bll.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneCore.DevTest.Bll.Dto
{
    public class ResponseDto
    {
        public StatusResponse Status { get; set; }
        public object Response { get; set; }
        public string Message { get; set; }
    }
}
