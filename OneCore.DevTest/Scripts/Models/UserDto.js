﻿class userDto {
    constructor(username, email, password, passwordConfirm, gender, status = '', id = 0) {
        this.id = id;
        this.Username = username;
        this.Email = email;
        this.Password = password;
        this.PasswordConfirm = passwordConfirm;
        this.Gender = gender;
        this.Status = status;
    }
}