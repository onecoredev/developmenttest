﻿app.controller('userController', ['$scope', '$location', '$routeParams', 'userService', function ($scope, $location, $routeParams, userService) {
    $scope.redirectTo = function (target = null) {
        hideResponseDialog();

        if (target)
            $location.path(target);
        else
            history.back();
    }

    $scope.deleteUser = function (Id) {

        showProgressBar();
        userService.get(Id).$promise.then(function (result) {
            setResponserValues($scope, result.Status, result.Message);
            hideProgressBar();
            showResponseDialog();
        }, function (error) {
            setResponserValues($scope, error.status, error.data.Message, error.data.ModelState);
            hideProgressBar();
            showResponseDialog();
        });
    }

    $scope.createUser = function (username, email, password, passwordConfirm, gender) {
        var user = new userDto(username, email, password, passwordConfirm, gender);

        showProgressBar();

        userService.save(user).$promise.then(function (result) {
            setResponserValues($scope, result.Status, result.Message);
            hideProgressBar();
            showResponseDialog();
        }, function (error) {
            setResponserValues($scope, error.status, error.data.Message, error.data.ModelState);
            hideProgressBar();
            showResponseDialog();
        });
    }

    $scope.editUser = function (username, email, password, passwordConfirm, gender) {
        var user = new userDto(username, email, password, passwordConfirm, gender, $scope.status, $scope.id);

        showProgressBar();

        userService.update(user).$promise.then(function (result) {
            setResponserValues($scope, result.Status, result.Message);
            hideProgressBar();
            showResponseDialog();
        }, function (error) {
            setResponserValues($scope, error.status, error.data.Message, error.data.ModelState);
            hideProgressBar();
            showResponseDialog();
        });
    }

    $scope.getUser = function (id) {
        userService.get({ id: id }).$promise.then(function (result) {
            hideProgressBar();
            if (result.Status === 0) {
                $scope.id = result.Response.Id;
                $scope.username = result.Response.UserName;
                $scope.email = result.Response.Email;
                $scope.password = result.Response.password;
                $scope.status = result.Response.Status;
                $scope.gender = result.Response.Gender;
            } else {
                setResponserValues($scope, result.Status, result.Message);
                hideProgressBar();
                showResponseDialog();
            }
        }, function (error) {
            setResponserValues($scope, error.status, error.data.Message, error.data.ModelState);
            hideProgressBar();
            showResponseDialog();
        });
    }

    $scope.page.setTitle('Usuarios - OneCore');

    if ($('#users-table').length > 0) {
        renderUsersTable();
    }

    if ($routeParams.id) {
        showProgressBar();
        $scope.getUser($routeParams.id);
    }
}]);