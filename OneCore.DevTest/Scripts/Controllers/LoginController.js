﻿app.controller('loginController', ['$scope', '$location', 'loginService', function ($scope, $location, loginService) {
    $scope.page.setTitle('Iniciar sisión- OneCore');

    $scope.login = function (username, password) {
        var login = new userLogin(username, password);

        showProgressBar();

        loginService.save(login).$promise.then(function (result) {
            hideProgressBar();

            if (result.Status === 0) {
                $location.path(`/User/${result.Response.Id}`);
            } else {
                setResponserValues($scope, result.Status, result.Message)
                showResponseDialog();
            }
        }, function (error) {
            setResponserValues($scope, error.status, error.data.Message, error.data.ModelState)
            hideProgressBar();
            showResponseDialog();
        })
    }
}]);
