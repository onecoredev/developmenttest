﻿app.factory('userService', ['$resource', function ($resource) {
    return $resource('api/Users/:id', {}, {
        update: { method: 'PUT' }
    });
}]);