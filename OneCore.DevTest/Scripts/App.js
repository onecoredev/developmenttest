﻿var app = angular.module('devTestApp', ['ngRoute', 'ngResource']);

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/', {
            pageTitle: 'OneCore - Development Test',
            bodyClass: 'body-login',
            controller: 'loginController',
            templateUrl: 'Views/LoginView.html',
            css: ['Content/LoginStyle.css']
        })
        .when('/Users', {
            controller: 'userController',
            templateUrl: 'Views/UsersView.html'
        })
        .when('/User/Create', {
            controller: 'userController',
            templateUrl: 'Views/UserCreateView.html'
        })
        .when('/User/:id', {
            controller: 'userController',
            templateUrl: 'Views/UserInfoView.html',
        })
        .when('/User/Edit/:id', {
            controller: 'userController',
            templateUrl: 'Views/UserEditView.html'
        })
        .otherwise({
            redirectTo: '/'
        })
}]);

app.run(['$rootScope', '$location', function ($rootScope, $location) {
    $rootScope.page = {
        setTitle: function (pageTitle) {
            this.title = pageTitle;
        }
    }

    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.page.setTitle(current.$$route.pageTitle);
        $rootScope.bodyClass = current.$$route.bodyClass;
        $rootScope.css = current.$$route.css;
    });
}]);