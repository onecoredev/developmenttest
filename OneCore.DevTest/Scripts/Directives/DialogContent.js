﻿app.directive('dialogContent', function () {
    return {
        restrict: 'E',
        scope: {
            status: '=',
            message: '=',
            errors: "=",
            redirect: '=',
            path: '='
        },
        templateUrl: 'Views/Directives/DialogContent.html' 
    }
});