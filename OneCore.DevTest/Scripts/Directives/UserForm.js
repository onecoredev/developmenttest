﻿app.directive('userForm', function () {
    return {
        restrict: 'E',
        scope: {
            username: '=',
            email: '=',
            password: "=",
            confirm: '=',
            gender:'='
        },
        templateUrl: 'Views/Directives/UserForm.html'
    }
});