﻿app.directive('progressBar', function () {
    return {
        restrict: 'E',
        templateUrl: 'Views/Directives/IndeterminateProgresBar.html'
    }
});