﻿var showProgressBar = function () {
    showModal('#waitBarDialog');
}

var hideProgressBar = function () {
    hideModal('#waitBarDialog');
}

var showResponseDialog = function () {
    showModal('#responseDialog');
}

var hideResponseDialog = function () {
    hideModal('#responseDialog');
}

var showModal = function (id) {
    $(id).modal('show');
}

var hideModal = function (id) {
    $(id).modal('hide');
}

var setResponserValues = function (scope, responseStatus, responseMessage, errorObjects = []) {
    scope.responseStatus = responseStatus;
    scope.responseMessage = responseMessage;
    scope.errorObjects = errorObjects;
}