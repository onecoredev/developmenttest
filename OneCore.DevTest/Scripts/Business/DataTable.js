﻿var renderUsersTable = function () {
    $('#users-table').DataTable({
        responsive: true,
        ajax: {
            url: 'api/Users',
            dataSrc: ''
        },
        //data: [{ "Id": 1, "Email": "andresfer1987@hotmail.com", "UserName": "andresfer1987", "Password": "", "Status": true, "Gender": "Male", "CreationDate": "2018-10-12T15:11:04.747" }],
        columns: [
            { data: 'Id' },
            { data: 'UserName' },
            { data: 'Email' },
            { data: 'Gender' },
            { data: 'Status' },
            {
                data: null,
                render: function (data) {
                    switch (data.Status) {

                        case "Si": return `<a class="btn btn-link" href="#!/User/Edit/${data.Id}">Actualizar</a> <button type="button" onClick="deleteUser(${data.Id})" class="btn btn-link text-danger js-action">Inactivar</button>`;

                        case "No": return `<a class="btn btn-link" href="#!/User/Edit/${data.Id}">Actualizar</a>`;
                        default: value = "Error";
                            break;
                    }
                }
            }
        ],
        search: {
            searchPlaceholder: 'Buscar'
        }
    });


    deleteUser = function (id) {
        console.log("Click");
        $.ajax({
            url: "api/Users/" + id,
            method: "DELETE",
            contentType: "application/json; charset=utf-8",
            success: function () {
                $('#users-table').DataTable().ajax.reload();
            },
            error: function (error) {
                alert("Ha Ocurrido Un Error Al Borrar El Usuario: " + error);
            }

        });
    }
}