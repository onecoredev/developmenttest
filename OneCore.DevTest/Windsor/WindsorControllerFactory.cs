﻿using OneCore.DevTest.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace OneCore.DevTest.Windsor
{
    public class WindsorControllerFactory : IHttpControllerActivator
    {
        private readonly IocHelper _windsorContainer;

        public WindsorControllerFactory(IocHelper windsorContainer)
        {
            _windsorContainer = windsorContainer;
        }

        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            var controller = (IHttpController)_windsorContainer.Resolve(controllerType);

            request.RegisterForDispose(
            new Release(
                () => _windsorContainer.Release(controller)));

            return controller;
        }

        private class Release : IDisposable
        {
            private readonly Action _release;

            public Release(Action release)
            {
                _release = release;
            }

            public void Dispose()
            {
                _release();
            }
        }
    }
}