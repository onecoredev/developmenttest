﻿using AutoMapper.Configuration;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using OneCore.DevTest.Bll;
using OneCore.DevTest.Bll.Configuration;
using OneCore.DevTest.Bll.Helpers;
using OneCore.DevTest.Controllers;
using OneCore.DevTest.Dal.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneCore.DevTest.Windsor
{
    public class WebApiInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<UsersController>().ImplementedBy<UsersController>().LifestylePerWebRequest(),
                Component.For<IUserBO>().ImplementedBy<UserBO>().LifestyleSingleton(),
                Component.For<DevTest_Entities>().ImplementedBy<DevTest_Entities>().LifestyleSingleton(),
                Component.For<IUserHelper>().ImplementedBy<UserHelper>().LifestyleSingleton(),
                Component.For<IEncryptHelper>().ImplementedBy<EncryptHelper>().LifestyleSingleton(),
                Component.For<IConfigWebApi>().ImplementedBy<ConfigWebApi>().LifestyleSingleton(),
                Component.For<SessionController>().ImplementedBy<SessionController>().LifestylePerWebRequest()
            );
        }
    }
}