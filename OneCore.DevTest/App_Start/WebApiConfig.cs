﻿using OneCore.DevTest.Ioc;
using OneCore.DevTest.Windsor;
using OneCore.DevTest.App_Start;
using OneCore.DevTest.Models.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace OneCore.DevTest
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de API web

            // Rutas de API web
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var jsonFormatter = new JsonMediaTypeFormatter();
            config.Services.Replace(typeof(IContentNegotiator), new JsonContentNegotiator(jsonFormatter));

            //Dependency register via windsor
            WindsorConfig.RegisterDependencies();
            config.Services.Replace(typeof(IHttpControllerActivator), new WindsorControllerFactory(IocHelper.Instance));

            //Validation model attribute
            config.Filters.Add(new ValidateModelAttribute());
        }
    }
}
