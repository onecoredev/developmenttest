﻿using Castle.MicroKernel.Registration;
using OneCore.DevTest.Ioc;
using OneCore.DevTest.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneCore.DevTest.App_Start
{
    public class WindsorConfig
    {
        public static void RegisterDependencies()
        {
            try
            {
                var windsorInstallers = new List<IWindsorInstaller>
                {
                   new WebApiInstaller()
                };

                IocHelper.Instance.Install(windsorInstallers.ToArray());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}