﻿
using AutoMapper;
using OneCore.DevTest.Bll;
using OneCore.DevTest.Bll.Dto;
using OneCore.DevTest.Bll.Enum;
using OneCore.DevTest.Bll.Helpers;
using OneCore.DevTest.Dal.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OneCore.DevTest.Controllers
{

    public class UsersController : ApiController
    {
        private IUserBO _userBO;
        private IUserHelper _userHelper;

        public UsersController()
        {

        }

        public UsersController(IUserBO userBO, IUserHelper userHelper)
        {
            _userBO = userBO;
            _userHelper = userHelper;
        }

        // GET api/Users
        public IEnumerable<UserDto> GetUsers()
        {
            return _userBO.GetUsers();
        }

        // GET api/Users/1
        public ResponseDto GetUser(int id)
        {

            var user = _userBO.GetUser(id);

            if (user == null)
                return new ResponseDto
                {
                    Status = StatusResponse.Error,
                    Message = "Usuario No Encontrado",
                    Response = null
                };

            return new ResponseDto
            {
                Status = StatusResponse.Success,
                Message = "Usuario Encontrado",
                Response = user
            };
        }

        //POST api/Users
        [HttpPost]
        public ResponseDto CreateUser(UserDto user)
        {
            try
            {

                if (!ModelState.IsValid)
                    throw new HttpResponseException(HttpStatusCode.BadRequest);

                return _userBO.CreateUser(user);
            }
            catch (Exception ex)
            {

                return new ResponseDto
                {
                    Status = StatusResponse.Error,
                    Message = string.Concat("Error: ", ex.Message),
                    Response = null
                };
            }


        }

        //PUT api/Users
        [HttpPut]
        public ResponseDto UpdateUser(UserDto user)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new HttpResponseException(HttpStatusCode.BadRequest);

                var userUpdate = _userHelper.FindUser(user.Id);

                if (userUpdate == null)
                    return new ResponseDto
                    {
                        Status = StatusResponse.Success,
                        Message = "El Usuario No Existe",
                        Response = null
                    };


                _userBO.UpdateUser(user);
                return new ResponseDto
                {
                    Status = StatusResponse.Success,
                    Message = "Usuario Actualizado Correctamente",
                    Response = user
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto
                {
                    Status = StatusResponse.Error,
                    Message = string.Concat("Error: ", ex),
                    Response = null
                };
            }
        }

        [HttpDelete]
        public ResponseDto DeleteUser(int id)
        {
            try
            {
                var user = _userHelper.FindUser(id);

                if (user == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);


                _userBO.DeleteUser(id);
                return new ResponseDto
                {
                    Status = StatusResponse.Success,
                    Message = "Usuario Inactivado Correctamente",
                    Response = user
                };
            }
            catch (Exception ex)
            {
                return new ResponseDto
                {
                    Status = StatusResponse.Error,
                    Message = string.Concat("Error: ", ex.Message),
                    Response = null
                };

            }
        }




    }


}

