﻿using OneCore.DevTest.Bll;
using OneCore.DevTest.Bll.Dto;
using OneCore.DevTest.Bll.Enum;
using OneCore.DevTest.Bll.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OneCore.DevTest.Controllers
{
    public class SessionController : ApiController
    {
        private IUserBO _userBO;
        private IUserHelper _userHelper;

        public SessionController(IUserBO userBO, IUserHelper userHelper)
        {
            _userBO = userBO;
            _userHelper = userHelper;
        }

        [HttpPost]
        public ResponseDto LoginUser([FromBody]UserLoginDto user)
        {
            try
            {
                if (string.IsNullOrEmpty(user.UserName) || string.IsNullOrEmpty(user.Password))
                    throw new HttpResponseException(HttpStatusCode.BadRequest);

                return _userBO.LoginUser(user.UserName, user.Password);
            }
            catch (Exception ex)
            {
                return new ResponseDto
                {
                    Status = StatusResponse.Error,
                    Message = string.Concat("Error: ", ex),
                    Response = null
                };
            }

        }
    }
}
